Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1997-2023, Eddie Kohler.
License: GPL-2

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: FSFULLR

Files: ar-lib
 compile
 depcomp
 missing
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: cfftot1/cfftot1.cc
 cfftot1/maket1font.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 2014-2019, 2023, Jonas Smedegaard <dr@jones.dk>
 2014, Vasudev Kamath <kamathvasudev@gmail.com>
 2003-2012, Claire Connelly <cmc@debian.org>
License: GPL-3+

Files: glyphlist.txt
Copyright: 2002, 2010, 2015, Adobe Systems Incorporated.
License: Apache-2.0

Files: include/lcdf/clp.h
Copyright: 1997-2023, Eddie Kohler, ekohler@gmail.com
License: GPL-2

Files: include/lcdf/md5.h
Copyright: 1995, 1996, 1998, 1999, Free Software Foundation, Inc.
License: GPL-2+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: libefont/*
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: liblcdf/*
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: liblcdf/clp.c
Copyright: 1997-2023, Eddie Kohler, ekohler@gmail.com
License: GPL-2

Files: liblcdf/md5.c
Copyright: 1995, 1996, 1998, 1999, Free Software Foundation, Inc.
License: GPL-2+

Files: mmafm/main.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: mmpfb/main.cc
 mmpfb/myfont.cc
 mmpfb/t1minimize.cc
 mmpfb/t1rewrit.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: otfinfo/otfinfo.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: otftotfm/automatic.cc
 otftotfm/dvipsencoding.cc
 otftotfm/glyphfilter.cc
 otftotfm/kpseinterface.c
 otftotfm/metrics.cc
 otftotfm/otftotfm.cc
 otftotfm/secondary.cc
 otftotfm/uniprop.cc
 otftotfm/util.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: t1dotlessj/t1dotlessj.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: t1lint/cscheck.cc
 t1lint/t1lint.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: t1rawafm/t1rawafm.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: t1reencode/t1reencode.cc
 t1reencode/util.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: t1testpage/t1testpage.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: ttftotype42/ttftotype42.cc
Copyright: 1997-2023, Eddie Kohler
License: GPL-2+

Files: Makefile.in lcdf-typetools.spec otfinfo/otfinfo.1
Copyright:
 1997-2023  Eddie Kohler <ekohler@gmail.com>
License: GPL-2

Files: cfftot1/Makefile.in libefont/Makefile.in liblcdf/Makefile.in mmafm/Makefile.in mmpfb/Makefile.in otfinfo/Makefile.in otftotfm/Makefile.in t1dotlessj/Makefile.in t1lint/Makefile.in t1rawafm/Makefile.in t1reencode/Makefile.in t1testpage/Makefile.in ttftotype42/Makefile.in
Copyright:
 1994-2021  Free Software Foundation, Inc.
License: FSFULLR~Makefile.in

Files: include/lcdf/hashmap.cc include/lcdf/vector.cc liblcdf/error.cc liblcdf/straccum.cc liblcdf/string.cc liblcdf/vectorv.cc
Copyright:
 2001-2023  Eddie Kohler <ekohler@gmail.com>
 2001-2003  International Computer Science Institute
 1999-2006  Massachusetts Institute of Technology
 2008-2009  Meraki, Inc.
License: Expat
